$(document).ready(function() {
  "use strict";

  var vid_class;

  $('.youtube').click(function() {
    var vid_class = $(this).attr('class').replace('youtube ', '');

    switch (vid_class) {
      case 'main-vid':
        var vid_id = "xHF4OZX8wGk";
        break;

      case 'vidrow1cell1':
        var vid_id = "xkkGVotHxXQ";
        break;

      case 'vidrow1cell2':
        var vid_id = "fAWkfAle-TI";
        break;

      case 'vidrow1cell3':
        var vid_id = "X91V4r5Cr9c";
        break;

      case 'vidrow2cell1':
        var vid_id = "bN57gqrJKwU";
        break;

      case 'vidrow2cell2':
        var vid_id = "1kPebpp3o_c";
        break;

      case 'vidrow2cell3':
        var vid_id = "Bxjh5p-cf48";
    }

    $(this).empty();
    $(this).append('<iframe class="ytframe" width="100%" height="100%" src="https://www.youtube.com/embed/' + vid_id + '?autoplay=1&amp;rel=0&amp;showinfo=0&amp;color=white&amp;theme=light&amp;wmode=transparent" frameborder="0" allowfullscreen></iframe>');
  });

  $('.youtube.main-vid').click(function() {

    $(this).empty();
    $(this).append('<iframe class="ytframe--main" width="100%" height="100%" src="https://www.youtube.com/embed/xHF4OZX8wGk?autoplay=1&amp;rel=0&amp;showinfo=0&amp;color=white&amp;theme=light&amp;wmode=transparent" frameborder="0" allowfullscreen></iframe>');
  });



  // init matchHeight
  $('div.callouts > div > h2').matchHeight();
});
