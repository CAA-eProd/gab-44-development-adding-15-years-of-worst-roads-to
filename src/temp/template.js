// import hero from '../hero.html'
import sub_nav from '../sub-nav.html'
import body from '../body.html'

let content = `
  ${sub_nav}
  ${body}
`;

let mainContent = document.querySelector(".main-content");

mainContent.outerHTML = content;
